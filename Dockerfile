FROM docker.io/library/debian:bookworm-slim AS builder

ENV DEBIAN_FRONTEND="noninteractive" \
    ZOMBOID_APPID="380870" \
    ZOMBOID_HOME="/opt/zomboid" \
    JAVA_OPTIONS="-XX:+UseG1GC -XX:MaxGCPauseMillis=200" \
    LC_ALL="en_US.UTF-8" \
    LC_CTYPE="en_US.UTF-8"

LABEL maintainer="Damian Gerow <dwg@flargle.io>"
LABEL name="project-zomboid"

RUN sed -i -e 's/\(^Components: .*\)/\1 non-free/' /etc/apt/sources.list.d/debian.sources
RUN apt -y update
RUN apt -y --no-install-recommends upgrade
RUN apt -y --no-install-recommends install locales software-properties-common
RUN dpkg-reconfigure locales --frontend noninteractive
RUN dpkg --add-architecture i386
RUN apt -y update
RUN apt -y --no-install-recommends install perl-modules

RUN echo steam steam/question select "I AGREE" | debconf-set-selections
RUN apt -y --no-install-recommends install lib32gcc-s1 steamcmd

RUN apt -y clean
RUN rm -rf /var/lib/lists/* /tmp/* /var/cache/apt/*

RUN /usr/games/steamcmd \
   +force_install_dir "${ZOMBOID_HOME}" \
   +login anonymous \
   +app_update ${ZOMBOID_APPID} validate \
   +quit

RUN useradd -c "Zomboid" -M -d ${ZOMBOID_HOME} -s /sbin/nologin -U zomboid
RUN install -m 0755 -o zomboid -g zomboid -d /Zomboid

EXPOSE 16261/udp

VOLUME [ "/data" ]

USER zomboid
WORKDIR /opt/zomboid

ENTRYPOINT [ "/opt/zomboid/start-server.sh", "-Djava.awt.headless=true", "-Duser.home=/data", "-Xms2G", "-Xmx8G", "--" ]
CMD [ "-servername", "zoibers", "-adminpassword", "zoibers", "-port", "16261" ]
